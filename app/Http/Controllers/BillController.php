<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBillRequest;
use App\Models\Bill;
use App\Models\ChiTietDonHang;
use App\Models\SanPham;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BillController extends Controller
{
    public function admin_index()
    {
        return view('new_admin.page.hoa_don.index');
    }

    public function getData()
    {
        $data = Bill::all();

        return response()->json([
            'bill'  => $data,
        ]);
    }

    public function store(CreateBillRequest $request)
    {
        DB::beginTransaction(); // Bắt đầu giao dịch trong cơ sở dữ liệu
        try {
            $list_cart = $request->list_cart; // Lấy danh sách mặt hàng từ yêu cầu
            // Lọc ra các mặt hàng đã được chọn trong giỏ hàng
            foreach ($request->list_cart as $key => $value) {
                if (!isset($value->check)) {
                    array_splice($list_cart, $key, 1);
                }
            }

            if (count($list_cart) > 0) { // Nếu danh sách mặt hàng không rỗng
                $customer = Auth::guard('customer')->user(); // Lấy thông tin khách hàng đăng nhập

                // Tạo một đơn hàng mới trong cơ sở dữ liệu
                $bill = Bill::create([
                    'ship_fullname' => $request->ship_fullname,
                    'ship_address' => $request->ship_address,
                    'ship_phone' => $request->ship_phone,
                ]);

                $check = true; // Biến kiểm tra tính hợp lệ của đơn hàng
                $totalAmount = 0; // Tổng số tiền của đơn hàng

                // Lấy danh sách mặt hàng từ giỏ hàng của khách hàng
                $cartItems = ChiTietDonHang::where('id_customer', $customer->id)->get();

                // Duyệt qua các mặt hàng từ giỏ hàng của khách hàng
                foreach ($cartItems as $key => $value) {
                    foreach ($list_cart as $value_cart) {
                        if ($value->id == $value_cart['id']) { // Nếu mặt hàng từ giỏ hàng trùng với mặt hàng đã chọn
                            $product = SanPham::find($value->id_san_pham); // Tìm thông tin sản phẩm

                            // Kiểm tra sản phẩm có sẵn và đủ số lượng để mua không
                            if ($product && $product->is_open == true && $product->so_luong >= $value->so_luong_mua) {
                                $unitPrice = $product->don_gia_khuyen_mai === 0 ? $product->don_gia_ban : $product->don_gia_khuyen_mai;
                                $value->ten_san_pham = $product->ten_san_pham;
                                $value->don_gia_mua = $unitPrice;
                                $value->hinh_anh = $product->hinh_anh;
                                $value->id_don_hang = $bill->id; // Liên kết mặt hàng với đơn hàng mới
                                $value->save(); // Lưu thông tin mặt hàng

                                $totalAmount += $unitPrice * $value->so_luong_mua; // Tính tổng số tiền của đơn hàng
                            } else {
                                $check = false; // Đánh dấu đơn hàng không hợp lệ
                            }
                        }
                    }
                }


                // Cập nhật thông tin của đơn hàng và lưu vào cơ sở dữ liệu
                $bill->bill_name = "HDQL" . (158935 + $bill->id);
                $bill->bill_total = $totalAmount;
                $bill->save();

                // // Hoàn thành giao dịch nếu đơn hàng hợp lệ, ngược lại rollback
                // if ($check) {
                //     DB::commit();
                // } else {
                //     DB::rollBack();
                // }
            }
            // Trả về thông tin về việc tạo đơn hàng qua JSON response
            return response()->json([
                'status' => true,
                'message' => 'Đã tạo đơn hàng thành công!',
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage("Lỗi"));
        }
    }


    public function index()
    {
        return view('client_new.pages.list_order');
    }

    public function listOrder()
    {
        $cutommer = Auth::guard('customer')->user();
        $listOrder = Bill::join('chi_tiet_don_hangs', 'bills.id', 'chi_tiet_don_hangs.id') //////
            ->join('san_phams', 'chi_tiet_don_hangs.id_san_pham', 'san_phams.id')
            ->join('danh_mucs', 'san_phams.danh_muc_id', 'danh_mucs.id')
            ->where('id_customer', $cutommer->id)
            ->select('chi_tiet_don_hangs.*', 'bills.bill_total', 'danh_mucs.ten_danh_muc')
            ->get();

        return response()->json(['listOrder' => $listOrder]);
    }

    public function listBill()
    {
        $listBill = Bill::all();

        return response()->json(['listBill' => $listBill]);
    }
}
